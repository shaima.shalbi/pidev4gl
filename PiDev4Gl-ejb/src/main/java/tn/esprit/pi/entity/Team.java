package tn.esprit.pi.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TEAM")
public class Team implements Serializable{
	
	private static final long serialVersionUID = -357738161698377833L;

	
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY
	)
	@Column(name="TI_ID")
	int id;

	
	
	@Column(name="T_TEAMNAME")
	String nameTeam;



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getNameTeam() {
		return nameTeam;
	}



	public void setNameTeam(String nameTeam) {
		this.nameTeam = nameTeam;
	}



	public Team() {
		super();
	}

}
