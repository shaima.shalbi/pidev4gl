package tn.esprit.pi.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="WORKDAY")
public class WorkDay implements Serializable{
	private static final long serialVersionUID = -357738161698377833L;

	
	
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY
	)
	@Column(name="WD_ID")
	int id ;
	
	
	@Column(name="WD_DATE")
	Date date;


	@ManyToOne
	@JoinColumn(name="FK_T_ID")
	Task task; 
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "T_WORKDAY_TIMESHEET",
	joinColumns={@JoinColumn(name="WORK_ID")},
	inverseJoinColumns={@JoinColumn(name ="TIMESHEET_ID")})
	private Set<TimeSheet> timesheets; 
	
	
	public Task getTask() {
		return task;
	}


	public void setTask(Task task) {
		this.task = task;
	}


	public Set<TimeSheet> getTimesheets() {
		return timesheets;
	}


	public void setTimesheets(Set<TimeSheet> timesheets) {
		this.timesheets = timesheets;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	@Temporal(TemporalType.DATE)
	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public WorkDay() {
		super();
	}
	
	
	

}
