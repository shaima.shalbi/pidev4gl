package tn.esprit.pi.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import tn.esprit.pi.entity.Departement;



@Entity
@Table(name="EMPLOYE")
public class Employe implements Serializable{
	
	
private static final long serialVersionUID = -357738161698377833L;

	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	@Column(name="E_ID")
	int id;
	@Column(name="E_LOGIN")
	String login;
	@Column(name="E_PASSWORD")
	String password;
	@Column(name="E_NAME")
	String name ;
	@Column(name="E_LASTNAME")
	String lastName ;
	@Column(name="E_ADDRESS")
	String address ;
	@Column(name="E_AGE")
	String age ; 
	@Column(name="E_MAIL")
	String mail ;
	@Column(name="E_DEPARTEMENT")
	@Enumerated(EnumType.STRING)
	Departement departement ;
	@Column(name="E_PROJET")
	String projet ;
	@Column(name="E_TEAM")
	String team ;
	@Column(name="E_ROLE")
	@Enumerated(EnumType.STRING)
	Role role;
	@Column(name="E_IMAGE")
	String image ; 
	@Column(name="E_NBRJOURCONGES")
	int nbrJourConges;
	@Column(name="E_SALAIRE")
	double salaire;
	
	
	

	@OneToMany(mappedBy="employe" ,  cascade =  {CascadeType.PERSIST, CascadeType.REMOVE})
	private Set<Commentaire> commentaire;
	
	@OneToMany(mappedBy="employe" ,  cascade =  {CascadeType.PERSIST, CascadeType.REMOVE})
	private Set<Condidature> condidature;
	
	@ManyToOne
	@JoinColumn(name="id_bus")
	private Bus bus;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public Employe() {
		super();
	}
	public Employe(String login, String password, String name, String lastName, String address, String age, String mail,
			Departement departement, String projet, String team, Role role, String image,
			double salaire) {
		super();
		this.login = login;
		this.password = password;
		this.name = name;
		this.lastName = lastName;
		this.address = address;
		this.age = age;
		this.mail = mail;
		this.departement = departement;
		this.projet = projet;
		this.team = team;
		this.role = role;
		this.image = image;
		this.nbrJourConges = 18;
		this.salaire = salaire;
	}
	public Employe(int id, String login, String password, String name, String lastName, String address, String age,
			String mail, Departement departement, String projet, String team, Role role, String image,
			int nbrJourConges, double salaire) {
		super();
		this.id = id;
		this.login = login;
		this.password = password;
		this.name = name;
		this.lastName = lastName;
		this.address = address;
		this.age = age;
		this.mail = mail;
		this.departement = departement;
		this.projet = projet;
		this.team = team;
		this.role = role;
		this.image = image;
		this.nbrJourConges = nbrJourConges;
		this.salaire = salaire;
	}



















	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Set<Commentaire> getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(Set<Commentaire> commentaire) {
		this.commentaire = commentaire;
	}

	public Set<Condidature> getCondidature() {
		return condidature;
	}

	public void setCondidature(Set<Condidature> condidature) {
		this.condidature = condidature;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Departement getDepartement() {
		return departement;
	}

	public void setDepartement(Departement departement) {
		this.departement = departement;
	}

	public String getProjet() {
		return projet;
	}

	public void setProjet(String projet) {
		this.projet = projet;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getNbrJourConges() {
		return nbrJourConges;
	}

	public void setNbrJourConges(int nbrJourConges) {
		this.nbrJourConges = nbrJourConges;
	}

	public double getSalaire() {
		return salaire;
	}

	public void setSalaire(double salaire) {
		this.salaire = salaire;
	}
	
	
	
	
}

