package tn.esprit.imputation.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class Commentaire {


	private static final long serialVersionUID = -357738161698377833L;

	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY
	)
	@Column(name="C_ID")
	int id;
	
	
	@Column(name="C_TEXTE")
	String texte;

	
	@ManyToOne
	@JoinColumn(name="FK_TP_ID")
	TravauxPratiques travauxPratiques; 

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getTexte() {
		return texte;
	}


	public void setTexte(String texte) {
		this.texte = texte;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public Commentaire() {
		super();
	}



	
	
}
