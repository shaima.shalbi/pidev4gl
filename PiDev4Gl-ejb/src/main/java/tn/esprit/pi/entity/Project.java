package tn.esprit.pi.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="T_PROJECT")
public class Project implements Serializable{


	private static final long serialVersionUID = -357738161698377833L;

	
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY
	)
	@Column(name="P_ID")
	int id; 
	
	@Column(name="P_NAME")
	String name;
	
	
	@Column(name="P_STARTDATE")
	Date startDate;
	
	@Column(name="P_DURATION")
	int duration;
		
	
	@Column(name="P_DESCRIPTION")
	String description;

	
	@Enumerated(EnumType.STRING)
	private Technologie technologie;
	
	@OneToMany(mappedBy="projet" , cascade =  {CascadeType.PERSIST, CascadeType.REMOVE})
	private Set<Task> tasks;
	
	@OneToMany(mappedBy="projet" , cascade =  {CascadeType.PERSIST, CascadeType.REMOVE})
	private Set<TimeSheet> timesheet;
	
	
	public Team getTeam() {
		return team;
	}


	public Technologie getTechnologie() {
		return technologie;
	}


	public void setTechnologie(Technologie technologie) {
		this.technologie = technologie;
	}


	public void setTeam(Team team) {
		this.team = team;
	}


	@OneToOne
	@JoinColumn(name="FK_T_ID")
	private Team team; 
	
	public Set<TimeSheet> getTimesheet() {
		return timesheet;
	}


	public void setTimesheet(Set<TimeSheet> timesheet) {
		this.timesheet = timesheet;
	}


	public Set<Task> getTasks() {
		return tasks;
	}


	public void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}




	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	@Temporal(TemporalType.DATE)
	public Date getStartDate() {
		return startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public int getDuration() {
		return duration;
	}


	public void setDuration(int duration) {
		this.duration = duration;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Project() {
		super();
	}
	
	
	
}
